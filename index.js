module.exports = class Translator {
    constructor(translations, defaultLanguage) {
        // support both 'en' and ref('en')
        if (defaultLanguage.value === undefined) {
            defaultLanguage = { value: defaultLanguage };
        }
        this._translations = translations;
        this._language = defaultLanguage;
        this._fallback = defaultLanguage.value;
    }

    language() {
        return this._language.value;
    }

    languages() {
        return Object.fromEntries(
            Object.entries(this._translations)
                .map(([code, translations]) => {
                    return [code, translations.language];
                })
        );
    }

    switchLanguage(code) {
        this._language.value = code;
    }

    detectLanguage() {
        if (typeof navigator === 'undefined') { return this._fallback; }
        for (let language of navigator.languages) {
            if (this._translations.hasOwnProperty(language)) {
                return language;
            }
            if (language.length === 5 && this._translations.hasOwnProperty(language.substring(0, 2))) {
                return language.substring(0, 2);
            }
        }
        return this._fallback;
    }

    translate(key, params = {}) {
        let translation = this.get(key);
        if (translation === undefined) {
            translation = this.get(key, this._fallback);
        }

        return this.applyParams(
            translation,
            params,
        );
    }

    get(key, language = undefined) {
        let value = this._translations[language || this._language.value];
        for (let part of key.split('.')) {
            value = value[part];
            if (value === undefined) { return undefined; }
        }
        return value;
    }

    has(key) {
        return this.get(key) !== undefined;
    }

    applyParams(value, params = {}) {
        if (!value) { return value; }
        return Array.isArray(value)
            ? value.map(v => this._applyParamsString(v, params))
            : this._applyParamsString(value, params);
    }

    _applyParamsString(value, params = {}) {
        if (!value) { return value; }
        return value.replace(new RegExp(/{(.+?)}/, 'g'), (m, c) => {
            let r = params;
            for (let part of c.split('.')) {
                r = r[part];
                if (r === undefined) { return undefined; }
            }
            return r;
        })
    }
};
