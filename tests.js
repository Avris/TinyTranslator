const test = require('baretest')('TinyTranslator'),
    assert = require('assert'),
    TinyTranslator = require('./index');

const translations = {
    en: {
        language: 'english',
        hello: 'hello!',
        colours: {
            red: 'red',
            green: 'green',
            blue: 'blue',
        },
        count: '{count} items',
        config: 'here is a config value: {CONFIG.FOO.BAR}',
    },
    pl: {
        language: 'polski',
        hello: 'cześć!',
        colours: {
            red: 'czerwone',
            green: 'zielone',
        },
        count: 'liczba wpisów: {count}',
        config: 'oto wartość z konfiguracji: {CONFIG.FOO.BAR}',
    },
}

const CONFIG = {
    FOO: {
        BAR: 'baz',
    },
}

const translator = new TinyTranslator(translations, 'en');

test('manage languages', async function() {
    assert.deepEqual(
        translator.languages(),
        {
            en: 'english',
            pl: 'polski',
        }
    );

    global.navigator = {languages: ['pl', 'en']};
    assert.equal(translator.detectLanguage(), 'pl');

    global.navigator = {languages: ['de']};
    assert.equal(translator.detectLanguage(), 'en');

    global.navigator = {languages: ['de', 'pl-PL']};
    assert.equal(translator.detectLanguage(), 'pl');
});

test('english', async function() {
    assert.equal(translator.language(), 'en');
    assert.equal(
        translator.translate('language'),
        'english',
    );
    assert.equal(
        translator.translate('hello'),
        'hello!',
    );
    assert.equal(
        translator.translate('colours.green'),
        'green',
    );
    assert.equal(
        translator.translate('colours.blue'),
        'blue',
    );
    assert.equal(
        translator.has('colours.blue'),
        true,
    );
    assert.equal(
        translator.translate('count', {count: 5}),
        '5 items',
    );
    assert.equal(
        translator.translate('config', {CONFIG}),
        'here is a config value: baz',
    );
});

test('polski', async function() {
    translator.switchLanguage('pl');
    assert.equal(translator.language(), 'pl');
    assert.equal(
        translator.translate('language'),
        'polski',
    );
    assert.equal(
        translator.translate('hello'),
        'cześć!',
    );
    assert.equal(
        translator.translate('colours.green'),
        'zielone',
    );
    assert.equal(
        translator.translate('colours.blue'),
        'blue',
    );
    assert.equal(
        translator.has('colours.blue'),
        false,
    );
    assert.equal(
        translator.translate('count', {count: 5}),
        'liczba wpisów: 5',
    );
    assert.equal(
        translator.translate('config', {CONFIG}),
        'oto wartość z konfiguracji: baz',
    );
});

test.run();
