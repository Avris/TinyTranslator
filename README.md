# 👅 TinyTranslator

> a minimalistic translator library

## usage

given the example translations, `en.js`:

    module.exports = {
        language: 'english',
        hello: 'hello!',
        colours: {
            red: 'red',
            green: 'green',
            blue: 'blue',
        },
        count: '{count} items',
    }

and `pl.js`:

    module.exports = {
        language: 'polski',
        hello: 'cześć!',
        colours: {
            red: 'czerwone',
            green: 'zielone',
        },
        count: 'liczba wpisów: {count}',
    }

you can initialise the translator like this:

    const TinyTranslator = require('avris-tinytranslator');

    const translator = new TinyTranslator({
        en: require('../translations/en.js'),
        pl: require('../translations/pl.js'),
    }, 'en');

the last parameter specifies a default language – it will be selected at the initialisation
and also used as a fallback if a translation is missing from another language.

    translator.translate('hello'); // 'hello!'
    translator.translate('colours.green'); // 'green'
    translator.translate('colours.blue'); // 'blue'
    translator.translate('count', {count: 5}); // '5 items'

you can manually select another language:

    translator.switchLanguage('pl');
    translator.translate('hello'); // 'cześć!'
    translator.translate('colours.green'); // 'zielone'
    translator.translate('colours.blue'); // 'blue' (fallback from english)
    translator.translate('count', {count: 5}); // 'liczba wpisów: 5'

or detect it automatically:

    translator.switchLanguage(translator.detectLanguage());

you can also make the translations **reactive**
by simply passing a [`ref('en')`](https://vuejs.org/api/reactivity-core.html#ref) object as the default language.

if you set a special translation key `language`,
it will be used to provide a dictionary like this via `translator.languages()`:

    {
        en: 'english',
        pl: 'polski',
    }

## development

    node tests.js     # run tests
    node homepage.js  # regenerate homepage from readme

## author & links

 - andrea vos
 - [avris.it](https://avris.it)
 - license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
 - [source code](https://gitlab.com/Avris/TinyTranslator/-/blob/main/index.js)
 - [npm package](https://www.npmjs.com/package/tinytranslator)
