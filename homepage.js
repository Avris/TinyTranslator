const fs = require('fs'),
    markdown = require( "markdown" ).markdown;

fs.mkdirSync(__dirname + '/public', { recursive: true })
fs.writeFileSync(
    __dirname + '/public/index.html',
    `<!DOCTYPE>
    <html>
        <head>
            <meta charset="utf-8"/>
            <title>TinyTranslator</title>
            <meta name="description" content="a minimalistic translator library">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link rel="shortcut icon" href="data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20100%20100%22%3E%3Ctext%20y%3D%22.9em%22%20font-size%3D%2290%22%3E%F0%9F%91%85%3C%2Ftext%3E%3C%2Fsvg%3E" type="image/svg+xml">
        </head>
        <body>
            ${markdown.toHTML(
                fs.readFileSync(__dirname + '/README.md').toString('UTF-8')
            )}
            <script defer data-domain="tinytranslator.avris.it" src="https://plausible.avris.it/js/script.js"></script>
        </body>
    </html>`.replace(/<a href="https:\/\//g, '<a target="_blank" rel="noopener" href="https://')
);
